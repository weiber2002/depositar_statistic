# Depth of Depositar Data
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/weiber2002%2Fdepositar_statistic/main)
author: Weipo hsin


## Introduction
In this book, we will explore Depositar Data. We have multiple data information and analyses of the datasets, projects, and topics in the Depositar repository, and we provide fixed output formats of the statistics as well as vivid graphs. The statistics analysis covers 5 categories, including [**Spatial**](./hellop/spatial_guide), [**Depositar guidence**](./hellop/depositar.ipynb), [**Metadata Analysis**](./hellop/avr_guide), [**Data analysis**](./hellop/largest_guide), and out-resource supported [**User Searching section**](./hellop/searching_user.ipynb) and [**Author Searching section**](./hellop/searching_author.ipynb)
Each part in this book could be executed independently after one slightly modifies the way of fetching data.

## Requirement
It's a book mainly written in Python and Markdown. It can be executed on any python-environment such as Colab, and Jupyter-lab to fetch the newest data. Furthermore, we have an additional inputting section available when executing on such environment. 

## Basic Dependencies
```
pip install -r requirements.txt
```
## For future developer
* We have no interactive compiling section due to the framework limitations of Gitlab. That means interactive elements like buttons, input rows are not supported on the website hosted by Gitlab. However, if one moves the code to Colab or any other Python environment, we can utilize this function.

* Certainly, updating statistics on the website automatically is crucial for such project. However, it's a bad news for future developers that we didn't develop this function successfully. We suggest future users utilize pipeline schedule with a great-writed .ci file to implement such critical function.

* One should be careful of how the data are fetched. We fetch data in [depositar.ipynb](depositar.ipynb) and create corresponding .json file, and other files fetch data from it. If one needs to implement any functions in a independent file, they could copy the code from [depositar.ipynb](depositar.ipynb) to implement data fetching.

* We don't have permissions to use api to get users information, though we can still see the data on the website if we directly type the url. Future developers may alternate the way of fetching the package related to users by using api.

## how to post an api of depositar 
https://data.depositar.io//api/3/action/     add action and parameter after this
ex: https://data.depositar.io//api/3/action/user_activity_list?id=...
more api can be found in [ckan](https://docs.ckan.org/en/2.9/api/index.html#)

## Searching sections
These code sections will not show up in the Jupyter-book but on the Gitlab, we provide basic information for searching users and searching authors, both can work independently